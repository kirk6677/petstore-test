<?php
declare(strict_types=1);

namespace Acme;
use Medoo\Medoo;

class BaseModel {

    public $pdo;
    public $filters;


    //construct PDO and tie Medoo ORM to it
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;

        $this->pdo = new Medoo([
            // Initialized and connected PDO object
            'pdo' => $pdo,

            // [optional] Medoo will have different handle method according to different database type
            'database_type' => 'mysql'
        ]);

    }

    //main getter function to grab data from database, we will pass all filter data through here with RAW sql
    public function get($raw=null)
    {
        return $this->pdo->select(
            "items",
            [
                "[>]item_types" => "item_type_id",
                "[>]pet_types" => "pet_type_id",
                "[>]pet_breeds" => "pet_breed_id",
                "[>]items_colors" => "item_color_id"
            ],
            [
                "item_name",
                "item_type_name",
                "item_color_name",
                "pet_type_name",
                "pet_breed_name",
                "pet_breed_lifespan",
                "item_pet_age",
                "item_price" => Medoo::raw('IF((pet_breed_lifespan/2) <= item_pet_age, item_discount_price, item_price)'),
                "item_discount_price",
                "item_size"
            ],
            Medoo::raw($raw)
        );
    }


    //filters, each method is based on Top level Pets, Toys, Carriers, or All Items, so
    //Technically I am treating it more like categories than filters from this we can pass the actual
    //$_GET variables in as a set of filters for the sub filtering.
    //Everything can be run through the getAllItems method but adding a few extras to make it easier to access to levels.
    public function getAllItems($filters){
        $raw = "WHERE 1=1 ".$this->getFilter($filters);
        return $this->get($raw);
    }

    public function getPets($filters){
        $raw = "WHERE item_type_id = 1".$this->getFilter($filters);
        return $this->get($raw);
    }

    public function getPetToys($filters){
        $raw = "WHERE item_type_id = 2".$this->getFilter($filters);
        return $this->get($raw);
    }

    public function getPetCarriers($filters){
        $raw = "WHERE item_type_id = 3".$this->getFilter($filters);
        return $this->get($raw);
    }

    public function printdebug($stuff){
        print '<pre>';
        print_r($stuff);
    }


    //The actual filter creation method takes filter and sort data and converts it to valid SQL
    //starting with the first AND statement after the WHERE which is used for the top level categories
    //Not the best way to do this but....
    public function getFilter($filters){
        $filter = "";
        $raw_price = "";
        if(is_array($filters)){

            //Do we need to sort?
            if(isset($filters['sort']) && isset($filters['sort_by'])){

                $sort = $filters['sort'];
                $sort_by = $filters['sort_by'];

                //Now remove the elements from the array and leave the rest of the filters
                //we will put it all together later
                unset($filters['sort']);
                unset($filters['sort_by']);


                //Build last part of raw query to add at the end
                $raw_sort = " ORDER BY $sort_by $sort";
            }


            //Do we need to filter by price? if so we have to take discounts into effect
            if(isset($filters['max_price']) && isset($filters['min_price'])){

                $maxprice = $filters['max_price'];
                $minprice = $filters['min_price'];

                //Now remove the elements from the array and leave the rest of the filters
                //we will put it all together later
                unset($filters['max_price']);
                unset($filters['min_price']);


            //Build last part of raw query to add at the end
            $raw_price = " IF((pet_breed_lifespan/2) <= item_pet_age, item_discount_price, item_price)<$maxprice AND IF((pet_breed_lifespan/2) <= item_pet_age, item_discount_price, item_price)>$minprice";
        }


            //finish the rest of the filter checks
            $filter = http_build_query($filters) . "\n";
            $filter = str_replace('&', ' AND ', $filter);
            $filter = str_replace('pet_type_id', 'items.pet_type_id', $filter);
            $filter = str_replace('min_lifespan=', 'pet_breed_lifespan>', $filter);
            $filter = str_replace('max_lifespan=', 'pet_breed_lifespan<', $filter);
            $filter = str_replace('min_age=', 'item_pet_age>', $filter);
            $filter = str_replace('max_age=', 'item_pet_age<', $filter);

            //Sometimes filter is empty but with space from http_build_query function
            $filter = trim($filter);

            //if we have a filter lets add the first AND
            if(!(empty($filter))){
                $filter = " AND " . $filter;
            }

            //if there is no normal filter but a price filter is present we still need the first AND
            if($raw_price && (empty($filter))){
                $filter = " AND ".$filter;
            }

            //Now add sorting to the end
            $filter = $filter.$raw_price.$raw_sort;

        }

        return $filter;
    }



}
