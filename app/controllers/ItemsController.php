<?php
declare(strict_types=1);

namespace Acme;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig_Environment;
use LucidFrame\Console\ConsoleTable;


class ItemsController
{

    public $baseModel;
    public $category = null;
    private $twig;



    public function __construct(BaseModel $baseModel, Twig_Environment $twig)
    {
        //start DB and View Engine
        $this->baseModel = $baseModel;
        $this->twig = $twig;
    }



    public function index($routeParams)
    {
        //$routeParams = Main Category i.e. [Pets, Pet Toys, Pet Carriers, All Items]
        //$request = Sub filters i.e. ['Dogs, Cats, Reptiles']
        //Trying not to use $_GET, $_POST, $_REQUEST

        $request = Request::createFromGlobals();

        //Start Building Filter Data based on url params
        //Category $routeParams; --- Sub Category $filters;

        $filters = $request->getQueryString();
        if (isset($filters)) {
            parse_str($request->getQueryString(), $filters);
        } else {
            $filters = "";
        }

        //check the params to see which method we need and then pass the filters i.e. Pets or Pet Toys

            switch(@$routeParams['category']) {
                case "Pets":
                    $items = $this->baseModel->getPets($filters);
                    break;
                case "AllItems":
                    $items = $this->baseModel->getAllItems($filters);
                    break;
                case "PetToys":
                    $items = $this->baseModel->getPetToys($filters);
                    break;
                case "PetCarriers":
                    $items = $this->baseModel->getPetCarriers($filters);
                    break;
                default:
                    $items = $this->baseModel->get();
            }

        //Time to load twig template for view, but lets see if we need to load HTTP version or CLI version.
        if (php_sapi_name() == "cli") {
            $this->clidisplay($items);
        } else {
            return new Response($this->twig->render('pages/index.html.twig', array('items' => $items)));
        }
    }



    public function clidisplay($items)
    {
        //this is to use a CLI rendering library to show the data in a nice way from the cli
        //Not sure why it would be needed for a pet store but the doc mentioned sorting from
        // Command line and I thought it was interesting to tinker with

        $table = new ConsoleTable();
        $table
            ->addHeader('Item')
            ->addHeader('Category')
            ->addHeader('Pet Type')
            ->addHeader('Pet Breed')
            ->addHeader('Color')
            ->addHeader('Pet Age')
            ->addHeader('Price')
            ->addHeader('Discounted Price')
            ->addHeader('LifeSpan')
            ->addHeader('Size');

        foreach ($items as $item) {
            $table->addRow()
                ->addColumn($item['item_name'])
                ->addColumn($item['item_type_name'])
                ->addColumn($item['pet_type_name'])
                ->addColumn($item['pet_breed_name'])
                ->addColumn($item['item_color_name'])
                ->addColumn($item['item_pet_age'])
                ->addColumn($item['item_price'])
                ->addColumn($item['item_discount_price'])
                ->addColumn($item['pet_breed_lifespan'])
                ->addColumn($item['item_size']);
        }

        $table->display();;


    }



    public function documentation()
    {
        return new Response("Documentation Page");
    }


}