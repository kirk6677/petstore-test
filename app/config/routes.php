<?php
return [
    ['GET', '/', ['Acme\ItemsController', 'index']],
    ['GET', '/index', ['Acme\ItemsController', 'index']],
    ['GET', '/index/{category}', ['Acme\ItemsController', 'index']],
    ['GET', '/documentation', ['Acme\ItemsController', 'documentation']],
];