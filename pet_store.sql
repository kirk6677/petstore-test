-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: pet_store
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `item_types`
--

DROP TABLE IF EXISTS `item_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_types` (
  `item_type_id` int(11) DEFAULT NULL,
  `item_type_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_types`
--

LOCK TABLES `item_types` WRITE;
/*!40000 ALTER TABLE `item_types` DISABLE KEYS */;
INSERT INTO `item_types` VALUES (1,'Pets'),(2,'Pet Toys'),(3,'Pet Carriers');
/*!40000 ALTER TABLE `item_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `item_id` int(11) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `item_type_id` int(11) DEFAULT NULL,
  `pet_type_id` int(11) DEFAULT NULL,
  `pet_breed_id` int(11) DEFAULT NULL,
  `item_color_id` int(11) DEFAULT NULL,
  `item_pet_age` int(11) DEFAULT NULL,
  `item_price` decimal(10,0) DEFAULT NULL,
  `item_discount_price` decimal(10,0) DEFAULT NULL,
  `item_size` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'Black Labrador Puppy',1,1,1,1,8,551,150,'Large'),(2,'Yellow Labrador Ret Puppy',1,1,1,4,1,550,150,'Large'),(3,'Red Tailed Boa',1,3,15,2,3,475,150,'Medium'),(4,'Kong Ball Chew Toy',2,1,0,2,0,20,20,'Small'),(5,'Brand X Heat Lamp',2,3,0,2,0,13,13,'Small'),(6,'Sphynx Kitten',1,2,8,5,2,799,329,'small'),(7,'Cat-Nip',2,2,0,5,0,5,5,'small'),(8,'Cat Carrier small',3,2,0,3,0,49,0,'small'),(9,'Cat Carrier Medium',3,2,0,3,0,49,0,'Medium'),(10,'Cat Carrier Large',3,2,0,3,0,49,0,'Large'),(11,'Lizard Cage Small',3,3,0,5,0,29,0,'Small'),(11,'Lizard Cage Medium',3,3,0,5,0,35,0,'Medium'),(12,'Large Dog Crate',3,1,0,5,0,99,0,'Large'),(13,'German Shephard Pup',1,1,3,1,1,650,200,'Large'),(14,'Adult German Shephard',1,1,3,1,6,650,200,'Large');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items_colors`
--

DROP TABLE IF EXISTS `items_colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items_colors` (
  `item_color_id` int(11) DEFAULT NULL,
  `item_color_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items_colors`
--

LOCK TABLES `items_colors` WRITE;
/*!40000 ALTER TABLE `items_colors` DISABLE KEYS */;
INSERT INTO `items_colors` VALUES (1,'Black'),(2,'Red'),(3,'Calico'),(4,'Yellow'),(5,'Brown');
/*!40000 ALTER TABLE `items_colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pet_breeds`
--

DROP TABLE IF EXISTS `pet_breeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pet_breeds` (
  `pet_breed_id` int(11) DEFAULT NULL,
  `pet_breed_name` varchar(255) DEFAULT NULL,
  `pet_breed_lifespan` int(11) DEFAULT NULL,
  `pet_type_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pet_breeds`
--

LOCK TABLES `pet_breeds` WRITE;
/*!40000 ALTER TABLE `pet_breeds` DISABLE KEYS */;
INSERT INTO `pet_breeds` VALUES (1,'Labrador Retriever',12,1),(2,'Labrador Retriever',12,1),(3,'German Shepherd',12,1),(4,'Doberman Pinscher',12,1),(5,'Pit Bull',14,1),(6,'Siamese',20,2),(7,'Siberian',15,2),(8,'Sphynx',13,2),(9,'Bengal',16,2),(10,'Persian',18,2),(11,'Bearded Dragon',8,3),(12,'Red-Eared Slider Turtle',20,3),(13,'Iguana',15,3),(14,'Russian Tortoise',50,3),(15,'Red Tail Boa',40,3);
/*!40000 ALTER TABLE `pet_breeds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pet_types`
--

DROP TABLE IF EXISTS `pet_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pet_types` (
  `pet_type_id` int(11) DEFAULT NULL,
  `pet_type_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pet_types`
--

LOCK TABLES `pet_types` WRITE;
/*!40000 ALTER TABLE `pet_types` DISABLE KEYS */;
INSERT INTO `pet_types` VALUES (1,'Dogs'),(2,'Cats'),(3,'Reptiles');
/*!40000 ALTER TABLE `pet_types` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-31 22:22:46
