<?php
declare(strict_types=1);

use FastRoute\Dispatcher;
use League\Container\Container;
use League\Container\ReflectionContainer;
use Dotenv\Dotenv;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

require_once dirname(__FILE__) . '/vendor/autoload.php';



//Use Symfony HttpFoundation to handle Requests and Responses
$request = Request::createFromGlobals();


//Dotenv to store DB credentials in the environment and any other config data needed
if (file_exists(__DIR__ . '/app/config/.env') !== true) {
    Response::create('Missing .env file (please copy .env.example).', Response::HTTP_INTERNAL_SERVER_ERROR)
        ->prepare($request)
        ->send();
    return;
}

$dotenv = Dotenv::create(__DIR__ . '/app/config/');
$dotenv->load();



//Using PHP League Container to help with DI
$container = new Container();

//Push the Model and Twig into Controller
$container->add(Acme\ItemsController::class)->addArgument(Acme\BaseModel::class)->addArgument(Twig_Environment::class);

//Push the PDO into Model
$container->add(Acme\BaseModel::class)->addArgument(PDO::class);

//Define the PDO
$container
    ->add(PDO::class)
    ->addArgument($_ENV['DB_CONN'])
    ->addArgument($_ENV['DB_USER'])
    ->addArgument($_ENV['DB_PASS'])
;

$container->add('Twig_Environment')->addArgument(new Twig_Loader_Filesystem(__DIR__ . '/app/views/'));

$container
    ->delegate(
    // Auto-wiring based on constructor typehints.
    // http://container.thephpleague.com/auto-wiring
        new ReflectionContainer()
    );

$controller = $container->get(Acme\ItemsController::class);


if (php_sapi_name() == "cli") {
// Impose a different router here for CMD
}


//Routes using FastRoute
$dispatcher = FastRoute\simpleDispatcher(function (FastRoute\RouteCollector $r) {
    $routes = require __DIR__ . '/app/config/routes.php';
    foreach ($routes as $route) {
        $r->addRoute($route[0], $route[1], $route[2]);
    }
});


//Now we can dispatch
$routeInfo = $dispatcher->dispatch($request->getMethod(), $request->getPathInfo());
switch ($routeInfo[0]) {
    case Dispatcher::NOT_FOUND:
        // No matching route was found.
        Response::create("404 Not Found", Response::HTTP_NOT_FOUND)
            ->prepare($request)
            ->send();
        break;
    case Dispatcher::METHOD_NOT_ALLOWED:
        // A matching route was found, but the wrong HTTP method was used.
        Response::create("405 Method Not Allowed", Response::HTTP_METHOD_NOT_ALLOWED)
            ->prepare($request)
            ->send();
        break;
    case Dispatcher::FOUND:
        // Fully qualified class name of the controller
        $fqcn = $routeInfo[1][0];

        // Controller method responsible for handling the request
        $routeMethod = $routeInfo[1][1];

        // Route parameters (ex. /Items/PetCarriers/{id})
        $routeParams = $routeInfo[2];

        // Obtain an instance of route's controller
        // Resolves constructor dependencies using the container
        $controller = $container->get($fqcn);

        // Generate a response by invoking the appropriate route method in the controller
        $response = $controller->$routeMethod($routeParams);
        if ($response instanceof Response) {

            // Send the generated response back to the user
            $response
                ->prepare($request)
                ->send();
        }
        break;
    default:
        // According to the dispatch(..) method's documentation this shouldn't happen.
        // But lets make sure.
        Response::create('Received unexpected response from dispatcher.', Response::HTTP_INTERNAL_SERVER_ERROR)
            ->prepare($request)
            ->send();
        return;
}

