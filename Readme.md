# Pet Store Test

Its Def not perfect but it does use OOP, Containers, Dependency Injection, Routers, Dispatching, Twig, a light weight ORM, and a few other things.

***
Get All Black Labs Ages 1-5 http://localhost/index/Pets?pet_breed_id=1&min_age=1&max_age=5

Get All Pet Toys http://localhost/index/PetToys

Get All Pet Toys and sort by price ASC http://localhost/index/PetToys?sort=ASC&sort_by=item_price {This also takes into account the breed average lifespan and if the pets age

Get All Pet Carriers for Dogs Under $100 http://localhost/PetCarriers?pet_type_id=1&min&price=0&max_price=100

Get All Pets that are black and cost 500 and up  http://localhost/index/Pets?item_color_id=1&min_price=500&max_price=5000

Get All Pets that are under $500 sort by price  http://localhost/index/Pets?min_price=0&max_price=500&sort=DESC&sort_by=item_price  {This also takes into account the breed average lifespan and if the pets age is >= the lifespan then the discounted price from the database is shown in the view
***


![Screen Shot](public/images/Screenshot-Pet-Store.png)




